<div class="form-group">
    {{ Form::label('label', $label, ['class' => 'form-label col-md-4'], false) }}
    <div class="col-md-8">
        <span>{{ $value }}</span>
    </div>
</div>